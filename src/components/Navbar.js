import React from 'react'
import { NavLink } from "react-router-dom";

const Navbar = () => {
return (
  <div className="navbar">
        <div className='navItems navItems1'>
              <NavLink to={'/'}><div className="circle"></div></NavLink>
        </div>
        
        <div className="navItems navuser">USER <div className='navItems' className="navguest">GUEST </div></div>
        <div className="navItems navsnipers">SNIPERS: <div className="number1">  0</div></div>
        <div className="navItems navsnipers">GUNNERS: <div className="number2">  0</div></div>
        <div className="navItems navsnipers">KAMIKAZES: <div className="number3">  0</div></div>
        <div className="navItems navminers">MINERS: <div className="number4">  0</div></div>
        <div className="navItems navtanks">JETS: <div className="number5">  0</div></div>
        
        <NavLink to={'/shop'}>
           <div className='navItems navshop'>SHOP</div>
        </NavLink>
        <div className='navItems' className="navcoins">ZED<p className="navcoincolor"> COIN </p> COUNT<div className="number5">0</div></div>

  
  {/*<circle cx="14.5" cy="14.5" r="14.5" stroke="blue"/>
  <circle cx="14.5" cy="14.5" r="14" fill='https://res.cloudinary.com/ckellytv/image/upload/v1569504252/zolzar_images/roach_uoctm5.png'/>*/}
</div>)

}

// export default Navbar


// import React from 'react'

// const Navbar = () => {
// return (
//     <div  <div className="navbar">
//         <div  className="circle">
//     <svg xmlns="http://www.w3.org/2000/svg" width="1419" height="50" viewBox="0 0 1700 31">

//   <text id="USER" transform="translate(55 4)" fill="#fff" font-size="14" font-family="Courier"><tspan x="0" y="0">USER</tspan></text>
//   <text id="GUEST" transform="translate(55 20)" fill="red" font-size="18" font-family="Courier"><tspan x="0" y="0">GUEST</tspan></text>
//   <text id="snipers:_05" data-name="snipers: 05" transform="translate(197 15)" fill="#fff" font-family="Courier"><tspan x="0" y="0">SNIPERS: </tspan><tspan y="0" fill="#ff9b04">05</tspan></text>
//   <text id="machine_gunners:_05" data-name="machine gunners: 05" transform="translate(381 15)" fill="#fff" font-family="Courier"><tspan x="0" y="0">MACHINE GUNNERS: </tspan><tspan y="0" fill="#ff049e">05</tspan></text>
//   <text id="kamikazes:_05" data-name="kamikazes: 05" transform="translate(665 15)" fill="#fff" font-family="Courier"><tspan x="0" y="0">KAMIKAZES: </tspan><tspan y="0" fill="#1cff36">05</tspan></text>
//   <text id="miners:_05" data-name="miners: 05" transform="translate(874 15)" fill="#fff" font-family="Courier"><tspan x="0" y="0">MINERS: </tspan><tspan y="0" fill="red">05</tspan></text>
//   <text id="Tanks:_05" data-name="Tanks: 05" transform="translate(1045 15)" fill="#fff" font-family="Courier"><tspan x="0" y="0">TANKS: </tspan><tspan y="0" fill="#00ffa7">05</tspan></text>
//   <text id="BUY_MORE_UNITS" data-name="BUY MORE UNITS" transform="translate(1193 15)" fill="#1CFF36" font-family="Arial-Black, Arial Black" font-weight="800"><tspan x="0" y="0">SHOP</tspan></text>
//     <text id="BUY_MORE_UNITS" data-name="BUY MORE UNITS" transform="translate(1393 15)" fill="white" font-family="Arial-Black, Arial Black" font-weight="800"><tspan x="0" y="0">BUY ZED COINS</tspan></text>
  
//   {/*<circle cx="14.5" cy="14.5" r="14.5" stroke="blue"/>
//   <circle cx="14.5" cy="14.5" r="14" fill='https://res.cloudinary.com/ckellytv/image/upload/v1569504252/zolzar_images/roach_uoctm5.png'/>*/}

// </svg>

// </div> 
// </div>)


// }

 export default Navbar