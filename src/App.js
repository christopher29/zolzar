import React from 'react';
import {BrowserRouter as Router,Route } from "react-router-dom"
//========================  COMPONENTS ==========================
import Landing from './containers/landing.js'
import LetsPlay from './containers/letsplay.js'
import Roll from './containers/roll.js'
import Awards from './containers/awards.js'
import RoundOne from './containers/roundone.js'
import ReportOne from './containers/reportone.js'
import PlayVideo from './containers/playvideo.js'
import Video from './containers/video.js'
import GameOver from './containers/gameover.js'
import CreateAccount from './containers/createaccount.js'
import WrongDetails from './containers/wrongdetails.js'
import AlreadyClaimed from './containers/alreadyclaimed.js'
import SuccessClickLink from './containers/successclicklink.js'
import Shop from './containers/shop.js'
import YouveSelected from './containers/youveselected.js'
import AwesomeCoins from './containers/awesomecoins.js'
import PayNow from './containers/paynow.js'
import FailTryAgain from './containers/failtryagain.js'
import SuccessContinue from './containers/successcontinue.js'
import SuccessCharacters from './containers/successcharacters.js'
import Navbar from './components/Navbar'




class App extends React.Component {
  state = {
    
  }
  render(){
  return (
     <Router>
         <Navbar/>
         <Route exact path='/' component={Landing}/>
         <Route path='/letsplay' component={LetsPlay}/>
         <Route path='/roll' component={Roll}/>
         <Route path='/awards' component={Awards}/>
         <Route path='/roundone' component={RoundOne}/>
         <Route path='/reportone' component={ReportOne}/> 
         <Route path='/playvideo' component={PlayVideo}/> {/*NO NAV BAR*/}
         <Route path='/video' component={Video}/> {/*NO NAV BAR*/}
         <Route path='/gameover' component={GameOver}/> 
         <Route path='/createaccount' component={CreateAccount}/> {/*NO NAV BAR*/}
         <Route path='/wrongdetails' component={WrongDetails}/> {/*NO NAV BAR*/}
         <Route path='/alreadyclaimed' component={AlreadyClaimed}/> {/*NO NAV BAR*/}
         <Route path='/successclicklink' component={SuccessClickLink}/> {/*NO NAV BAR*/}
         <Route path='/shop' component={Shop}/>
         <Route path='/youveselected' component={YouveSelected}/>
         <Route path='/awesomecoins' component={AwesomeCoins}/>
         <Route path='/paynow' component={PayNow}/> {/*NO NAV BAR*/}
         <Route path='/failtryagain' component={FailTryAgain}/> {/*NO NAV BAR*/}
         <Route path='/successcontinue' component={SuccessContinue}/> {/*NO NAV BAR*/}
         <Route path='/successcharacters' component={SuccessCharacters}/>  {/*NO NAV BAR*/}
     </Router>
  );
}
}

export default App;

// Form will not work without, onSubmit and onChange. Form will not work without onSubmit in form, and onchange, inside input 
