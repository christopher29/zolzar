
import React , { useState } from 'react'
import Axios from 'axios' 

const Register = () => {
  const [ form , setValues ] = useState({
    email    : '',
    password : '',
    password2: ''
  })
  const [ message , setMessage ] = useState('')
  const handleChange = e => {
       setValues({...form,[e.target.name]:e.target.value})
  }
  const handleSubmit = async (e) => {
    e.preventDefault()
    try{
      const response =  await Axios.post(`http://localhost:8080/users/register`,{
              email    : form.email,
          password : form.password,
          password2: form.password2
          })
          setMessage(response.data.message)
          //console.log(response)
    }
    catch( error ){
      console.log(error)
    }

  }
  return <div className="grid"><div className="father"><form className="app" onSubmit={handleSubmit}
               onChange={handleChange}>
           <label className='redhead'>EMAIL</label>
         <input className='redhead' name="email"/>
         <label className='redhead'>PASSWORD</label>
         <input className='redhead'name="password"/>
         <label className='redhead'>REPEAT PASSWORD</label>
         <input className='redhead'name="password2"/>
         <button className="register" >register</button>
         <div className='message'><h4>{message}</h4></div>
         </form></div></div>
       
}

export default Register


/*

import React from 'react';
import Checkbox from '../zolzar_images/checkbox.png';

class Landing extends React.Component {

state = {
  username: '',
  password: ''
}

handleChange = (e) => {

    this.setState({[e.target.name]: e.target.value})
    // if i type in something in username input, it will setstate ({ username: e.target.value})
     // if i type in something in password input, it will setstate ({ password: e.target.value})

}

  render(){
  return (
    <div>
    <div className="father">

      <form className="app" onSubmit={this.handleSubmit}>

        <div className='redhead'>ENTER EMAIL</div>
         <input className='auth_inputs' name = 'username' onChange= {this.handleChange}></input>

        <div className='redhead'>CREATE USERNAME</div>
         <input className='auth_inputs' name = 'password' onChange= {this.handleChange}></input>


<div className='redhead'>CREATE PASSWORD</div>
         <input className='auth_inputs' name = 'username' onChange= {this.handleChange}></input>

        <div className='redhead'>CONFIRM PASSWORD</div>
         <input className='auth_inputs' name = 'password' onChange= {this.handleChange}></input>

        <button className='loginbuttonstyles'>CREATE AN ACCOUNT</button>
        <div className="links_container">
        <h2 className='links' href="#signup">I AGREE TO TERMS</h2> 
          
</div>
 

      </form>


</div>
 </div>
  );
}
}

export default Landing;

*/


