import React , { useState } from 'react'
import Axios from 'axios' 

const Login = (props) => {
  const [ form , setValues ] = useState({
    email    : '',
    password : ''
  })
  const [ message , setMessage ] = useState('')
  const handleChange = e => {
       setValues({...form,[e.target.name]:e.target.value})
  }
  const handleSubmit = async (e) => {
    e.preventDefault()
    try{
          const response = await Axios.post(`http://localhost:8080/users/login`,{
            email:form.email,
            password:form.password
          })
          setMessage(response.data.message)
          if( response.data.ok ){
              localStorage.setItem('token',JSON.stringify(response.data.token)) 
              setTimeout( ()=> props.history.push('/secret-page'),2000)    
          }
          
    }
        catch(error){
          console.log(error)
        }
  }
  return <div className="minify father grid"><form className="app" onSubmit={handleSubmit}
             onChange={handleChange}
             className='form_container'>
        <div className='redhead'>EMAIL</div>    
         <input name="email"/>
         <div className='redhead'>PASSWORD</div>
         <input name="password"/>
         <button className='loginbuttonstyles'>LOGIN</button>
         <div className='links' className='loginbuttonstyles' className='message'><h4>{message}</h4></div>
          <div  className='links' href="#signup">SIGN UP</div>   
        <div onClick={()=>props.history.push('/letsplay')}
                className='links' href="roll">PLAY AS GUEST</div>
         </form>
                 <div className="links_container">
</div>

         </div>


}

export default Login

/*import React from 'react';

class Landing extends React.Component {

state = {
  username: '',
  password: ''
}

handleSubmit =(e)=> {
    e.preventDefault()
    console.log(this.state.username)
    console.log(this.state.password)
         

  }

handleChange = (e) => {

    this.setState({[e.target.name]: e.target.value})
    // if i type in something in username input, it will setstate ({ username: e.target.value})
     // if i type in something in password input, it will setstate ({ password: e.target.value})

}

  render(){
  return (
    <div className="father">

   
      <form className="app" onSubmit={this.handleSubmit}>

        <div className='redhead'>USERNAME</div>
         <input className='auth_inputs' name = 'username' onChange= {this.handleChange}></input>

        <div className='redhead'>PASSWORD</div>
         <input className='auth_inputs' name = 'password' onChange= {this.handleChange}></input>



        <button className='loginbuttonstyles'>LOG IN</button>

        <div className="links_container">

        <h2 className='links' href="#signup">SIGN UP</h2>   
        <h2 onClick={()=>this.props.history.push('/letsplay')}
               className='links' href="roll">PlAY AS GUEST</h2>
</div>
 

      </form>


</div>
  );
}
}

export default Landing;

// Form will not work without, onSubmit and onChange. Form will not work without onSubmit in form, and onchange, inside input */
