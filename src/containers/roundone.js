import React from 'react';
import ReactPlayer from 'react-player'

// import UIfx from 'uifx'
// import backtrack from '.containers/sounds/backtrack.mp3.icloud'

//const backtrack = new UIfx(
 // backtrack,
 // {
 //   volume: 0.4, // number between 0.0 ~ 1.0
 //   throttleMs: 100
 // }
//)

// ========= >>>>> ========== //

class RoundOne extends React.Component {
	constructor(props){
		super(props)
		this.state = {
     selPeople : props.location.state.selPeople,
     selAliens : props.location.state.selAliens,
     locations : props.location.state.locations,
     currentBattlefield: props.location.state.locations.currentLocation,
     background: ["earthbackground", "moonbackground", "zolzarbackground"]

   }
 }
 componentDidMount(){
  this.newBattle()
}

newBattle = () => {
 let battleResults
 const { selPeople , selAliens, locations } = this.state
 let aliensScore = 0, peopleScore = 0
 for(var i =0; i < 3 ; i++){
  peopleScore+=selPeople[i].power * selPeople[i].rate
  aliensScore+=selAliens[i].power * selAliens[i].rate
  if((selPeople[i].power * selPeople[i].rate) * 100 == (selAliens[i].power * selAliens[i].rate) * 100){
    selPeople[i].remainingHealth = 0
    selAliens[i].remainingHealth = 0 }else if((selPeople[i].power * selPeople[i].rate) * 100 > (selAliens[i].power * selAliens[i].rate) * 100){
      selPeople[i].remainingHealth = selPeople[i].remainingHealth - ((selAliens[i].power * selAliens[i].rate) * 100) / (selPeople[i].power * selPeople[i].rate)
      selAliens[i].remainingHealth = 0}else if((selPeople[i].power * selPeople[i].rate) * 100 < (selAliens[i].power * selAliens[i].rate) * 100){
        selPeople[i].remainingHealth = 0
        selAliens[i].remainingHealth = selAliens[i].remainingHealth - ((selPeople[i].power * selPeople[i].rate) * 100) / (selAliens[i].power * selAliens[i].rate)
      }}

      this.setState({selPeople , selAliens})
      if(peopleScore - aliensScore > 0){  
        battleResults = 'BATTLE WON! HOLD THE LINE! STEP BY STEP'
        locations.currentLocation+=1
      }else{
        battleResults = 'BATTLE LOST! SOMETIMES YOU HAVE TO MOVE BACK TO MOVE FORWARD'
        locations.currentLocation-=1 }
        console.log("battleResults ", battleResults)
        this.setState({selPeople , selAliens, battleResults, locations})
        if (battleResults == 'BATTLE LOST! SOMETIMES YOU HAVE TO MOVE BACK TO MOVE FORWARD' && locations.currentLocation == -1){
         setTimeout(()=>{this.props.history.push({pathname: '/gameover', state: {battleResults, locations}})},17000)}else if(
          battleResults == 'BATTLE WON! HOLD THE LINE! STEP BY STEP' && locations.currentLocation == 3){
          setTimeout(()=>{this.props.history.push({pathname: '/playvideo', state: {battleResults, locations}})},17000)}else{
            setTimeout(()=>{this.props.history.push({pathname: '/reportone', state: {battleResults, locations}})},17000)}

          } 

          render(){

            console.log('locations',this.state.locations)
            const renderPpl = this.state.selPeople.map((unit, i)=>{

              return  <div>

              <div key = {i} > 
              <img className="leftstills" src={unit.img}/>
              <div className="lefttext"> Power: {unit.power}, Rate: {unit.rate}</div>
              <div className="lefttext"> Surviving Health: {Math.floor(unit.remainingHealth)}</div>
              </div></div>
            })

            const renderAl = this.state.selAliens.map((unit, i)=>{

              return <div>

              <div key = {i}> 
              <img className="rightstills" src={unit.img}/>
              <div className="righttext"> Power: {unit.power}, Rate: {unit.rate}</div>
              <div className="righttext"> Surviving Health: {Math.floor(unit.remainingHealth)}</div>
              <div class="laser-beam"></div>
<div class="laser-beam red"></div>
<div class="laser-beam purple"></div>
<div class="laser-beam green"></div>
              </div></div>
            })

            return (
            <div className={this.state.background[this.state.currentBattlefield]}> 
              <div className="leftpics">{renderPpl}</div>
              <div className="rightpics">{renderAl}</div>
              <div className='location' style={{gridColumn: 2/-1, color: '#00FFE2', fontFamily: 'Edo SZ', display: 'contents'}}>BATTLEFIELD: {this.state.locations.locationsList[this.state.currentBattlefield]}</div>
              <div>
              <ReactPlayer width="0%" height="0%" url='https://res.cloudinary.com/ckellytv/video/upload/v1570394451/zolzar_images/cirmusica_ixrxje.mp4' playing />
              </div>
              </div>

              )}}

            export default RoundOne;

/*import React from 'react'; 

class App extends React.Component {
  state = { name:'Jason'}

  componentDidMount(){
    console.log('coming from componentDidMount')
    let that = this
    setTimeout(function(){ that.setState({name:'Robert'})}, 3000);
  }

  render() {
    console.log('coming from the render function')
    return (
      <div>
      <h1>{this.state.name}</h1>
      </div>
      );
  }
}
export default App;*/