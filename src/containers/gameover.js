import React from 'react';
import { images } from '../images'


class GameOver extends React.Component {
	constructor(props){
    super(props)
    this.state ={
    	warResults : props.location.state.battleResults
    }
 }

	render(){
		return (
				


<div className = "gameoverbackground">
<div className = "copygrid">
<div>
<img className = "copyleft" src={images.signuptoaccess} alt="signuptoaccess"/>
<img onClick={()=>this.props.history.push('/createaccount')} className = "turnbuttonlittle1" src={images.signupformore} alt="signupformore"/>
<img onClick={()=>this.props.history.push('/letsplay')} className = "turnbuttonlittle2" src={images.replayasguest} alt="replayasguest"/>
</div>
<img className = "copyright" src={images.copy} alt="copy"/>

</div>
</div>


)
	}
}

export default GameOver;