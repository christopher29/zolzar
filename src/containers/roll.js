import React from 'react';
import { images } from '../images'


class Roll extends React.Component {
  constructor(props){
    super(props)
    this.state = {
     battleResults : props.location.state.battleResults,
     locations : props.location.state.locations,
     currentBattlefield: props.location.state.locations.currentLocation

   }
 }



render(){

  if(this.state.locations){
    var locations = this.state.locations
  }else{
    var locations = {currentLocation: 0, locationsList: ['EARTH','MOON','ZOLZAR']}
  }

  var people = [
  {name:'snipers', power:10,rate:40, remainingHealth: 100, initial_amount: 100, img:'https://res.cloudinary.com/ckellytv/image/upload/v1570041737/snipers_auzhwf.png'},
  {name:'gunners', power:20,rate:50, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569969504/gunners_yvlf33.png'},
  {name:'kamikazes', power:100,rate:1, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855485/kamikazes_hvtexr.png'},
  {name:'miners', power:50,rate:50, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855286/miners_q7n6rs.png'},
  {name:'jets', power:80,rate:50, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855284/jets_cisr9i.png'},
  {name:'roachs', power:19,rate:19, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855286/roachs_qwdzar.png'},
  {name:'bulls', power: 80,rate:70,remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855285/bulls_rqo3wq.png'},
  {name:'scorpions', power:90,rate:59, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855287/scorpions_c68bzi.png'},
  {name:'wasps', power:89,rate:70, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855287/wasps_qy752i.png'},
  {name:'microbot', power:20,rate:20, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855286/microbot_vg91yc.png'},
  {name:'k-9', power:40,rate:30, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855286/K-9_vw05og.png'},
  {name:'cyborg', power:40,rate:39, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855285/cyborg_jy61zy.png'},
  {name:'g32', power:50,rate:49, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855285/g32_lbh3qk.png'},
  {name:'mother', power:80,rate:90, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855286/mother_lcdnzc.png'},
  {name:'teo', power:80,rate:80, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855287/teo_ghxjtb.png'},
  {name:'kaos', power:89,rate:80, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855286/kaos_ao9x5s.png'},
  {name:'minotaur', power:90,rate:80, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855286/minotaur_k2zs4s.png'},
  {name:'belatrix', power:88,rate: 88, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855285/belatrix_x3caeo.png'},
  {name:'nine', power:99,rate:99, remainingHealth: 999, initial_amount: 999, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855287/nine_j6napm.png'},
  {name:'kamikazer', power:100,rate:80, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1570307613/zolzar_images/kamikazer_y2eh9v.png'},
  {name:'krog', power:99,rate:90, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1570307613/zolzar_images/krog_wb4qnq.png'},
  {name:'kalzar', power:100,rate:90, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1570307643/zolzar_images/kalzar_wfkcqt.png'},
  {name:'aquariana', power:99,rate:99, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1570308507/zolzar_images/aquariana_xwhlvg.png'},
  {name:'spore', power:100,rate:99, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1570309741/zolzar_images/spore_ve7o9s.png'},
  {name:'loop', power:100,rate:100, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1570309294/zolzar_images/loop_i6q8io.png'},
  ]

  var aliens = [
  {name:'roachs', power:19,rate:19, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855286/roachs_qwdzar.png'},
  {name:'bulls', power: 20,rate:20,remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855285/bulls_rqo3wq.png'},
  {name:'scorpions', power:25,rate:85, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855287/scorpions_c68bzi.png'},
  {name:'wasps', power:70,rate:69, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855287/wasps_qy752i.png'},
  {name:'zolzar', power:100,rate:99, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855287/zolzar_l13wkr.png'},
  ]

  var cyborgs = [
  {name:'microbot',power:3,rate:5, remainingHealth: 100, initial_amount: 100,img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855286/microbot_vg91yc.png'},
  {name:'k-9',power:1,rate:5, remainingHealth: 100, initial_amount: 100,img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855286/K-9_vw05og.png'},
  {name:'cyborg',power:4,rate:4, remainingHealth: 100, initial_amount: 100,img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855285/cyborg_jy61zy.png'},
  {name:'g32',power:2,rate:4, remainingHealth: 100, initial_amount: 100,img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855285/g32_lbh3qk.png'},
  {name:'mother',power:5,rate:5, remainingHealth: 100, initial_amount: 100,img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855287/teo_ghxjtb.png'}
  ]

  var immortals = [
  {name:'teo',power:3,rate:5, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855287/teo_ghxjtb.png'},
  {name:'kaos',power:4,rate:5, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855286/kaos_ao9x5s.png'},
  {name:'minotaur',power:4,rate:5, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855286/minotaur_k2zs4s.png'},
  {name:'belatrix',power:5,rate:5, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855285/belatrix_x3caeo.png'},
  {name:'nine',power:5,rate:5, remainingHealth: 100, initial_amount: 100, img: 'https://res.cloudinary.com/ckellytv/image/upload/v1569855287/nine_j6napm.png'}
  ]

  var selectedUnitsPeople = []
  var selectedUnitsAliens = []
  var selectedUnitsCyborgs = []
  var selectedUnitsImmortals = []
  var specialCharacters = []


  let battleRandomizer = (army1, army2)=>{
    for(var i = 0; i <3; i++){
      var randomNumber = Math.floor(Math.random() * (25 - 0) + 0)
selectedUnitsPeople.push(army1[randomNumber])
var randomNumber = Math.floor(Math.random() * (5 - 0) + 0)
selectedUnitsAliens.push(army2[randomNumber])
}

this.props.history.push({
	pathname: '/awards',
	state: {
		selectedUnitsPeople,
		selectedUnitsAliens,
    locations
  }
})

}

return (

  <div className='roll_container'>
  <div><h4 className='letsrolltitle'>SCROLL DOWN & LET'S ROLL</h4></div> 
  <div className= "letsplay2">
  <img className='minify' src={images.snipers} alt="snipers"/>
  <img className='minify' src={images.gunners} alt="gunners"/>
  <img className='minify' src={images.kamikazes} alt="kamikazes"/>
  <img className='minify' src={images.miners} alt="miners"/>
  <img className='minify' src={images.jets} alt="jets"/>
  <img className='minify' src={images.bulls} alt="snipers"/>
  <img className='minify' src={images.scorpions} alt="gunners"/>
  <img className='minify' src={images.roachs} alt="kamikazes"/>
  <img className='minify' src={images.wasps} alt="miners"/>
  <img className='minify' src={images.microbot} alt="snipers"/>
  <img className='minify' src={images.k9} alt="gunners"/>
  <img className='minify' src={images.cyborg} alt="kamikazes"/>
  <img className='minify' src={images.g32} alt="miners"/>
  <img className='minify' src={images.mother} alt="jets"/>
  <img className='minify' src={images.teo} alt="snipers"/>
  <img className='minify' src={images.kaos} alt="gunners"/>
  <img className='minify' src={images.minotaur} alt="kamikazes"/>
  <img className='minify' src={images.belatrix} alt="miners"/>
  <img className='minify' src={images.nine} alt="jets"/>
  <img className='minify' src={images.kamikazer} alt="gunners"/>
  <img className='minify' src={images.krog} alt="miners"/>
  <img className='minify' src={images.kalzar} alt="jets"/>
  <img className='minify' src={images.aquariana} alt="jets"/>
  <img className='minify' src={images.spore} alt="jets"/>
  <img className='minify' src={images.loop} alt="jets"/>


  </div>

  <div onClick={()=>battleRandomizer(people,aliens,locations)}>
<div id="wrapper">
  <input id="secondroll" name="roll" type="checkbox"></input>
  <input id="roll" name="roll" type="checkbox"></input>

  
  <div id="platform">
    <div id="dice">
      <div className="side front">
        <div className="dot center"></div>
      </div>
      <div className="side front inner"></div>
      <div className="side top">
        <div className="dot dtop dleft"></div>
        <div className="dot dbottom dright"></div>
      </div>
      <div className="side top inner"></div>
      <div className="side right">
        <div className="dot dtop dleft"></div>
        <div className="dot center"></div>
        <div className="dot dbottom dright"></div>
      </div>
      <div className="side right inner"></div>
      <div className="side left">
        <div className="dot dtop dleft"></div>
        <div className="dot dtop dright"></div>
        <div className="dot dbottom dleft"></div>
        <div className="dot dbottom dright"></div>
      </div>
      <div className="side left inner"></div>
      <div className="side bottom">
        <div className="dot center"></div>
        <div className="dot dtop dleft"></div>
        <div className="dot dtop dright"></div>
        <div className="dot dbottom dleft"></div>
        <div className="dot dbottom dright"></div>
      </div>
      <div className="side bottom inner"></div>
      <div className="side back">
        <div className="dot dtop dleft"></div>
        <div className="dot dtop dright"></div>
        <div className="dot dbottom dleft"></div>
        <div className="dot dbottom dright"></div>
        <div className="dot center dleft"></div>
        <div className="dot center dright"></div>
      </div>
      <div className="side back inner"></div>
      <div className="side cover x"></div>
      <div className="side cover y"></div>
      <div className="side cover z"></div>
    </div>

  </div>


  </div>

</div>

</div>

  )
}
}

export default Roll;

/* class Awards extends React.Component {
	render(){
	const selPeople = this.props.location.state.selectedUnitsPeople
	const selAliens = this.props.location.state.selectedUnitsAliens

    const renderPpl = selPeople.map((unit, i)=>{
      return <div key ={i} className='selUnits'> 
      	<img className="minify" src={unit.img}/>
      	<div className='selUnitFooter'>Power: {unit.power}</div>
      	<div className='selUnitFooter'>Rate: {unit.rate}</div>
      	
      </div>
    })

let gotoBattle = ()=>{this.props.history.push({pathname: '/roundone', state: {selPeople, selAliens}})}

		return (
<div>
<div className='award-container'>

   <div className="white">YOU HAVE BEEN AWARDED</div>

<div className='awards_cont'>{renderPpl}
</div>
<div className="whitefight" onClick={()=> gotoBattle()}>FIGHT</div>
<svg onClick={()=> gotoBattle()} xmlns="http://www.w3.org/2000/svg" width="37" height="42" viewBox="0 0 37 42">
  <path id="Polygon_8" data-name="Polygon 8" d="M21,0,42,37H0Z" transform="translate(37) rotate(90)" fill="#fff"/>
</svg>
</div>
</div>

)}}

export default Awards;*/






