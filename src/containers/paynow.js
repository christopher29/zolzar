import React from 'react';


class PayNow extends React.Component {
	render(){
		return (
			
<div className="bluebelle">
<text id="LET_S_PLAY" data-name="LET’S PLAY" transform="translate(0 59)" fill="#00b4c3" font-size="30" font-family="Arial-Black, Arial Black" font-weight="500"><tspan x="0" y="0">Pay Now</tspan></text>
</div>

)
	}
}

export default PayNow;