import React from 'react';


// select 3/5 objects from army1 at random
// print results from army 1
// var people = [{name:'snipers',power:5,rate:1},{name:'machineguns',power:2,rate:5},{name:'kamikaze',power:5,rate:3},{name:'miners',power:4,rate:2},{name:'tanks',power:5,rate:5}]
// var randomNumber = Math.floor(Math.random() * (5 - 0) + 0)]
// var unitStrength = army1[randomNumber].power*army1[randomNumber].rate
// slect 3/5 objects from army2 at random
// let wars = (army1, army2, locations) => {
// 	let battle = (army1,army2) => {
// var army2Strength = 0
// var army1Strength = 0
// for (var i = 0; i < 3; i++)
// {
// var randomNumber = Math.floor(Math.random() * (5 - 0) + 0)
// var unitStrength = army1[randomNumber].power*army1[randomNumber].rate
// // battleP.push(army1[randomNumber]) gives you a random unit 
// army1Strength = army1Strength + unitStrength
// var randomNumber2 = Math.floor(Math.random() * (5 - 0) + 0)
// // army2[randomNumber] gives you a random unit 
// var unitStrength2 = army2[randomNumber2].power*army2[randomNumber2].rate
// army2Strength = army2Strength + unitStrength2
// }
// if (army1Strength === army2Strength){
// return 'Nobody Won! We Will Meet Again!'
// }
// else if (army1Strength > army2Strength){
// return 'Hold The Line My Fellow Humans! We Got This! Step By Step!'
// }
// else {
// return 'HaHa!! We Won This Battle! Victory Is Ours! Soon You Will Be Our Slaves'
// }
// }
// 	console.log('inside wars')
// var interstellarWars = true
// var currentLocation = 0
// // call the battle function, compare results with current location, either stop the game or change the locations
// // either loop or recursion, change the value of interstellar wars
// // add console.log(results of current battle)
// var battleOutcome = battle (army1,army2)
// while(interstellarWars = true){
// if (battleOutcome === 'HaHa!! We Won This Battle! Victory Is Ours! Soon You Will Be Our Slaves'){
//     console.log(battleOutcome,locations[currentLocation])
//   if (currentLocation===0){
//        interstellarWars = false
//       return 'Game Over! The People Lost!!'
//   }else{
//        currentLocation =currentLocation -1
//       battleOutcome=battle(army1,army2)
//    }
// }else if (battleOutcome === 'Nobody Won! We Will Meet Again!'){
//    console.log(battleOutcome,locations[currentLocation])
// battleOutcome=battle(army1,army2)
// }else if(battleOutcome=== 'Hold The Line My Fellow Humans! We Got This! Step By Step!'){
//     console.log(battleOutcome,locations[currentLocation])
//    if (currentLocation===locations.length-1){
//        interstellarWars = false
//        return 'Game Over! The Aliens Lost!!'
//    }else{
//        currentLocation = currentLocation +1
//        battleOutcome=battle(army1,army2)
//    }
// }
// }
// }


	// componentDidMount(){
	// 	console.log('did mount', this.props.location.state.selectedUnitsPeople[0].name, this.props.location.state.selectedUnitsPeople[0].power, this.props.location.state.selectedUnitsPeople[0].rate)
	// }


class Awards extends React.Component {
	render(){
	const selPeople = this.props.location.state.selectedUnitsPeople
	const selAliens = this.props.location.state.selectedUnitsAliens
  const locations = this.props.location.state.locations

    const renderPpl = selPeople.map((unit, i)=>{
      return <div key ={i} className='selUnits'> 
      	<img className="minify" src={unit.img}/>
      	<div className='selUnitFooter'>Power: {unit.power}</div>
      	<div className='selUnitFooter'>Speed: {unit.rate}</div>
        <div className='selUnitFooter'>Health: {unit.initial_amount}</div>
      	</div>
      
    })

let gotoBattle = ()=>{this.props.history.push({pathname: '/roundone', state: {selPeople, selAliens, locations}})}

		return (
<div>
<div className='award-container'>

   <div className="whiteawarded 'awards_cont">YOU HAVE BEEN AWARDED</div>

<div className='awards_cont'>{renderPpl}
</div>
<div className="whitefight" onClick={()=> gotoBattle()}>FIGHT<svg onClick={()=> gotoBattle()} xmlns="http://www.w3.org/2000/svg" width="20" height="42" viewBox="0 0 37 42">
  <path className="margin"id="Polygon_8" data-name="Polygon 8" d="M21,0,42,37H0Z" transform="translate(37) rotate(90)" fill="#fff"/>
</svg></div>
</div>
</div>

)}}

export default Awards;