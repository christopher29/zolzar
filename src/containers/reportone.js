import React from 'react';
import { images } from '../images'


class ReportOne extends React.Component {
	constructor(props){
		super(props)
		this.state = {
     locations : props.location.state.locations,
     battleResults : props.location.state.battleResults

   }
	}
	componentDidMount(){
		let {locations, battleResults } = this.state
		setTimeout(()=>{this.props.history.push({pathname: '/roll', state: { battleResults, locations}})},5000)
	}
	render(){
		return (
<div className="missionreportbackground">
<div className='whiteawarded2'>{this.props.location.state.battleResults}, NEXT LEVEL: {this.state.locations.locationsList[this.state.locations.currentLocation]}</div>
</div>

)
	}
}

export default ReportOne;