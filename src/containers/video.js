import React from 'react';
import { images } from '../images'
import ReactPlayer from 'react-player'


class Video extends React.Component {
  render(){

return <div>

<ReactPlayer width="100%" height="100%" url='https://res.cloudinary.com/ckellytv/video/upload/v1570360850/zolzar_images/Comp_1_1_ppn9qi.mp4' playing />
<img className='fixvideobutton' onClick={()=>this.props.history.push('/createaccount')} src={images.signupformore} alt="signupformore"/>
<img className='fixvideobutton'onClick={()=>this.props.history.push('/letsplay')} src={images.replayasguest} alt="replayasguest"/>
</div>

  }
}

export default Video;

