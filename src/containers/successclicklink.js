import React from 'react';
import { images } from '../images'


class SuccessClickLink extends React.Component {
  render(){
    return (
     <div>
<div onClick={()=>this.props.history.push('/')}><img src={images.successyoucreatedanaccount} alt="successyoucreatedanaccount"/></div>
<div className="goto">
<div className="goto" onClick={()=>this.props.history.push('/letsplay')}><img classname="goto" src={images.gotogame} alt="gotogame"/></div>
<div className="goto" onClick={()=>this.props.history.push('/shop')}><img  classname="goto" src={images.gotoshop} alt="gotoshop"/></div>
</div>
</div>

)
  }
}

export default SuccessClickLink;