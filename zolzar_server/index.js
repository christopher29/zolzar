// =============== require and run express in a single line
const app =require('express')();
// =============== add body parser ===============
const bodyParser= require('body-parser');
const mongoose = require('mongoose');
const port = process.env.port || 8080;
const cors = require('cors');
// =============== BODY PARSER SETTINGS =====================
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
// =============== DATABASE CONNECTION =====================
mongoose.set('useCreateIndex', true);
mongoose.connect(`mongodb://127.0.0.1/auth`, { useNewUrlParser: true } ,()=>{
    console.log('*** connected to mongodb ***');
});
//================ CORS ================================
app.use(cors());
// =============== ROUTES ==============================
const usersRoute = require('./routes/users.routes');
// =============== USE ROUTES ==============================
app.use('/users', usersRoute);

// =============== START SERVER =====================
app.listen(port, () => 
    console.log(`server listening on port ${port}`
));
