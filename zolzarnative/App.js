import React from 'react';
import { 
  StyleSheet, 
  Text, 
  View,
  TextInput,
  Button,
  Alert,
  AsyncStorage,
  ImageBackground,
  ScrollView,
  Picker
 } from 'react-native';
 import Swipeout from 'react-native-swipeout';
export default class App extends React.Component{
  state = {
    todo : '',
    todos: ['q','e']
  }
  async componentDidMount(){
    await this.getTodos()
  }
  getTodos = async () => {
    //await AsyncStorage.clear()
    const response = await AsyncStorage.getItem('todos')
    response = JSON.parse(response)
    //Alert.alert('ok')
   try{
      response !== null
      ? this.setState({todos: response})
      : await AsyncStorage.setItem('todos',JSON.stringify([]))  
   }
   catch(error){
     console.log(error)
   }
  }
  handlePress = () => {
    console.log('=========> hello')
    const { todo , todos } = this.state
    todos.push(todo)
    this.setState({todo:'',todos})
  }
  handleRemoveTodo = todo => {
    const { todos } = this.state
    const index = todos.findIndex( ele => ele === todo)
    todos.splice(index,1)
    this.setState({todos})
  }
  render(){
    var swipeoutBtns = [
      {
          text: 'Update',
          backgroundColor:'green',
          onPress: ( ) => Alert.alert('remember to create update fun')
      },
      {
          text: 'Delete',
          backgroundColor:'red',
          onPress: ( ) => handleRemoveTodo()
      }
      ]
      
    return (
      <ImageBackground source= {require('./assets/images/weatherapp.png')}
                      style={styles.container}>
        <View style={styles.formContainer}>
          <TextInput style={styles.inputStyle}
                     value={this.state.todo}
                     onChangeText={(text) => this.setState({todo:text})}/>
          <Button
              onPress={this.handlePress}
              title="Add Todo!"
              color="#841584"
         />
        </View>
        <ScrollView style={styles.todosContainer}>
          {/* <Image source= {require('./assets/images/Varenna-Lake-Como.jpg')}
                style={{width:100,height:100}}/> */}
                
             {
               this.state.todos.map( (ele,i) => {
                 return <Swipeout 
                            key={i}
                            right={swipeoutBtns}
                            autoClose={true}
                            backgroundColor = 'transparent'
                          >
                   <View  style={styles.todoContainer}>
                           <Text>{ele}</Text>
                           <Button
                                 onPress={()=>this.handleRemoveTodo(ele)}
                                 title="Remove Todo!"
                                 color="red"
                           />
                       </View>
               </Swipeout>
               })
             }  
        </ScrollView>
      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 30,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  formContainer: {
    borderWidth: 1,
    borderColor: 'black',
    height: '25%',
    width: '100%'
  },
  inputStyle: {
    borderWidth: 1,
    borderColor: 'black',
  },
  todosContainer: {
    borderWidth: 1,
    borderColor: 'black',
    height: '75%',
    width: '100%'
  },
  todoContainer: {
    width: '100%',
    borderWidth: 1,
    borderColor: 'black',
    justifyContent: 'space-between',
    flexDirection: 'row'
  }
});
















